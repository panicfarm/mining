# mining

Crypto Mining Probability Calculations

I made some calculations for mining Grin, that can be generalized for mining arbitrary cryptocurrencies.

The typical questions that arise are:

What is the probability $`P(T_m)`$ of solving at least 1 block in time $`T_m`$ given our hash rate $`H`$ and the total network hash rate $`H_t`$. A related question is:
given the minimum desired probability of mining at least one block in time $`T_m`$, what is the minimum  hash rate $`H`$ required? 

Say we are mining in the cloud, and we have a fixed budget. We can spend the budget slowly, or we can burn it within a couple of blocks (provided that the cloud host can provision 
enough hardware). In other words, how does $`P(T_m)`$ depend on $`T_m`$? What strategy is best for the maximizing probability of solving at least one block? (The answer is that it 
actually does not matter whether you spend the budget slow or fast, as long as our hash power is much less than the total hash power of the network, the condition will be derived
in formula (6)).

The calculations below will show how to answer these questions and demonstrate some basic probability theory.

Let us set up some notation:
The basic fact is that the probability of guessing a single block $`\alpha`$ is (1):
```math
\alpha = \dfrac{H}{H_t}   
```
where as defined above, $`H`$ is our hash rate and $`H_t`$ is the total network hash rate. The process of guessing a single block in probability theory is called a
Poisson Trial (the outcome of mining a block is independent of the outcomes of mining previous bocks). The probability of not guessing one block in a single trial is therefore $`1-\alpha`$. Suppose we do multiple trials (mine during multiple blocks), and when 
we guess the first block, we stop. 
If $`n`$ is the number of trials, then the probability of not guessing a block is thus $`(1-\alpha)^n`$. The probability of guessing (mining) at least one block is thus (2):
```math
P = 1 - (1-\alpha)^n                     
```
Suppose a block takes an average time $`T`$ to produce (10 minutes in the case of Grin). Then $`n=\dfrac{T_m}{T}`$, where $`T_m`$ is the time over which we spend our mining budget.
Further, suppose $`g`$ is the number of mining GPUs we have (or ASICs), $`k`$ is the cost of each device (GPU) per hour in USD, and $`H_1`$ is the hashrate of a single device.
Then $`B=gkT_m`$ and (2) becomes (3):
```math
P(T_m, H_t) = 1 - \left(1-\dfrac{BH_1}{kT_mH_t}\right)^{\dfrac{T_m}{T}}
```
Note that $`H_t=H+H_o = \dfrac{BH_1}{kT_m}+H_o`$ (4), i.e. the total network hashrate is our hashrate $`H`$ and the hashrate outside of our control $`H_o`$. Subsitituting (4) into (3)
we get the final formula for probability of mining at least one block given the budget B (5):
```math
P(T_m, H_o) = 1 - \left(1+\dfrac{1}{\gamma T_m}\right)^{-\dfrac{T_m}{T}}
```
where
```math
\gamma \equiv \dfrac{H_ok}{BH_1}
```
As I mentioned at the start, generally this probability depends on how fast (time $`T_m`$) we spend the budget $`B`$. Looking at the formula (5), we can take a limit (Taylor series) when 
$`\dfrac{1}{\gamma T_m} \ll 1`$
or in other words (subsitituting $`H=\dfrac{BH_1}{kT_m}`$ into this contition), when the hash power of others is much greater than our hash power (6)
```math
\dfrac{H_o}{H} \gg 1 
``` 
Taking this limit, (5) becomes (7):
```math
P(H_o) \approx 1-\exp\left(-\dfrac{H_1B}{H_okT}\right)
```
In this formula, $`T_m`$ cancelled, i.e. the probability of mining at least one block does not depend on how fast we spend the budget $`B`$.

Let's make some concrete estimates for the recent case of Grin, using the Python code from the src directory, that has formulas (5) and (7) programmed.
....
....
....

Conversely suppose we spec that we need to mine at least one block with the probability say of 0.99. What is the maximum network hash rate $`H_o`$ that we can handle with the budget $`B`$?
From (7) we get (8):
```math
H_o \approx -\dfrac{H_1B}{kT\log\left(1-P\right)}
```
For example taking the current values of Grin from 



Let's ask more advanced questions now. What is the probability of mining at least $`r`$ blocks given $`n`$ trials (mining for $`n`$ blocks). Let's call it $`P\left(m\geq{r}|n\right)`$.
The probability of guessing exactly $`m`$ blocks in $`n`$ trials is given by the Binomial Distribution (8):
```math
P(m|n) = \dfrac{n!}{m!\left(n-m\right)!}\alpha^m\left(1-\alpha\right)^{n-m}
```
where $`\alpha=\dfrac{H}{H_t}`$ is the probability of guessing one block, and m is in the range from 0 to n. By definition of a probability distribution, 
$`\sum_{m=0}^{n}{P(m|n)} = 1`$. This means that (9)
```math
P\left(m\geq{r}|n\right) = 1 - \sum_{m=0}^{r-1}{P(m|n)}
```
where $`P(m|n)`$ is given by (8).
Let's check this for $`P\left(m\geq{1}|n\right)`$ which we have already dervied in (2):
```math
P\left(m\geq{1}|n\right) = 1 - P(0|n) = 1 - (1-\alpha)^n
``` 
(I used the definition that $`0!\equiv1`$), so we get the same result as before.
The expected reward (the mean number of blocks that we are going to guess) is given by
```math
\mu_1 = \sum_{m=0}^{n}{mP(m|n)}
```
This is also the definition for the first moment of a distribution. For the Binomial Distribution, it is known that
```math
\mu_1 = n\alpha 
```
substituting for $`n`$ and $`H`$ we get (10)
```math
\mu_1 = \dfrac{T_m}{T}\dfrac{1}{1+\dfrac{H_o}{H}} = \dfrac{T_m}{T}\dfrac{1}{1+\gamma{T_m}}
```
Observe that in the low hash rate limit as defined before $`\gamma{T_m} \gg 1`$ the expected number of blocks mined again becomes independent of $`T_m`$:
```math
\mu_1 \approx \dfrac{1}{\gamma{T}}
```
For example for 
....







